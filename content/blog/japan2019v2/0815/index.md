---
title: More shopping (Omotesando & Shibuya)
date: "2019-08-15"
---

I met up with Jack and Cat at around 1230 at Harajuku station for a full day of
shopping. We walked down famous Takeshita Street to Omotesando where we had
planned to eat at Gomaya Kuki, a sesame ice cream store which advertises itself
as having the world's richest sesame ice cream.

![Omotesando: a cool Evangelion shirt that I saw on the way to eat sesame ice
cream.](./IMG_20190815_130837.jpg)
![Gomaya Kuki, Omotesando: we got the tasting plate of six different flavours.](./IMG_20190815_131643.jpg)
![It's hard to eat while holding that thing.](./recording-sesame-eating.jpeg)
![I liked the "Rich Black" flavour the most. The "Triple Rich" flavours were
less refreshing tasting, and actually the "Triple Rich White" flavour was
comparable to the taste and texture of peanut butter.](./IMG_20190815_131646.jpg)
![You can also add more sesame seeds and sesame oil. The oil complemented the
"Triple Rich" flavours really well, by making them go down smoother.](./IMG_20190815_132102.jpg)

Afterwards, we made our way to Lattest, a cafe deep in Omotesando's backstreets
that also happens to be where former Terrace House member Mizuki Shida
occasionally works. Unfortunately, Mizuki wasn't there 😔.

![Lattest, Omotesando: Jack and Cat got the 500 JPY "Lattest", a shot pulled
over cold milk. The cookie was nice too.](./IMG_20190815_134815.jpg)
![ I got a double espresso for 400 JPY and it was great, probably
ranking among the top 20% of espressos that I've had in Sydney.](./IMG_20190815_135104.jpg)

After having a delicious espresso, we went to Imabari Towel because Cat wanted
to buy some good Japanese towels and had done her research. Since I had only
brought a beach towel to Japan, and the towels felt so great in the shop, I
convinced myself to buy one too.
[This page](http://imabaritoweljapan.com/quality/index.html) validated my
decision when I did some post-purchase research, even though it's there for
marketing purposes.

![Towels.](./IMG_20190815_144445.jpg)
![Small towels.](./IMG_20190815_144451.jpg)
![Our towels.](./IMG_20190815_144515.jpg)

We window shopped down Omotesando's pedestrian-only Cat Street, stopping at
Luke's Lobster to get a lobster roll. We eventually made it to Shibuya where we
did a bit more shopping and had dinner at CoCoICHI, a curry chain which I had
been meaning to try.

![Luke's Lobster, Omotesando: you either order regular or US size.](./IMG_20190815_151612.jpg)
![Don't tell me what to do.](./IMG_20190815_151617.jpg)
![I got the US size (right), which disappointingly only had a bit of extra
lobster. I wouldn't have minded more bread too since it was my first proper meal
of the day.](./IMG_20190815_151928.jpg)

![Shibuya: a really narrow building.](./IMG_20190815_161048.jpg)
![It's melting!](./IMG_20190815_161304.jpg)
![Shibuya crossing not looking too busy.](./IMG_20190815_161706.jpg)
![LOFT, Shibuya: adult sandpit!](./IMG_20190815_172528.jpg)
![Cool looking showerheads.](./IMG_20190815_174836.jpg)

![CoCo ICHIBANYA, Shibuya: it's like customising your bubble tea.](./IMG_20190815_182256.jpg)
![I got pork katsu and (much needed) vegetables, 400g rice, regular spice. I was
so full after this, but it was just what I needed since I was actually really
hungry after barely eating all day.](./IMG_20190815_182637.jpg)

![Tokyu Hands, Shibuya: tempting, I do want some colourful socks.](./IMG_20190815_190622.jpg)
![Okay I'm sold.](./IMG_20190815_190627.jpg)
![Taking the stairs never felt better.](./IMG_20190815_190858.jpg)
![This is probably what they wear at theme parks.](./IMG_20190815_192643.jpg)
![I really liked this.](./IMG_20190815_194300.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="wobbling-my-fat.mp4" type="video/mp4">
  </video>
  <figcaption>
    This made me feel the need to pee.
  </figcaption>
</figure>

I got home at 10 and Elbert and I explained grammar to each other; he explained
linguistic grammar, and I explained computer science (formal) grammar. Elbert
actually slept before me while I stayed up until 6am to push out my 2019-08-10
blog post.

![Home, Ootsuka: checking out my socks at home. Crushing them actually does feel
a bit like crushing paper. Based on one preliminary wear they feel pretty good.](./IMG_20190815_224042.jpg)

The weather today was actually very manageable, probably since the sun wasn't
out. At times the breeze was _almost_ cool. I really enjoyed hanging out with
Jack and Cat.

##### Waking hours

1130–3000
