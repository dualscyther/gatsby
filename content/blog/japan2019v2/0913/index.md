---
title: A long day of shopping and eating
date: "2019-09-13"
---

I met with Bob and Christine at 1200 to have lunch at Sushi Kidoguchi. On the
walk there from Harajuku station I noticed that my calves and quads were sore,
probably from running and doing stairs yesterday. I finally found out that the
chef was named Ryosuke and he was friendly as always so we had a good time. He
cracked a few jokes about how I spilled the miso soup last time, and acted
surprised when I actually finished it this time.

![Sushi Kidoguchi, Omotesando: I finally remembered to take a photo of the
squid piece.](./IMG_20190913_120614.jpg)
![Ryosuke-san explained that for women, he served the egg without rice.](./IMG_20190913_122514.jpg)

I wasn't quite full yet and I was curious how good some of the other sushi was
so I made a few extra orders. It wasn't cheap; the lunch set was about ~3200 JPY
but the extra pieces averaged to 950 JPY each, although to be fair they were
probably some of the more expensive ones that you could order.

![Akagai (ark shell). Bob and I had one each. It looked amazing, and tasted
better than the one that I had at my local sushi train. Sweet, although it's
hard to describe the texture, sort of like abalone I
guess.](./IMG_20190913_124207.jpg)
![I prodded Bob and Christine to order ootoro since they hadn't properly
tried it before.](./IMG_20190913_124444.jpg)
![Kohada (gizzard shad). I'm either acquiring the taste or it was better than
the piece that I had at Kyubey. I still wouldn't call it amazing
though.](./IMG_20190913_124527.jpg)
![ ](./IMG_20190913_124817.jpg)

![Nanaya Matcha, Shibuya: Ryosuke-san recommended this place for having the
"strongest matcha ice cream" in the world. The flavours here are Japanese
black tea and Number 7 strength matcha (rated from 1-7. You had to pay
something like a 150 JPY premium if you wanted the Number 7 strength, so that
left a bit of a bad taste in my mouth. It did taste significantly stronger
than Bob's Number 6 though and I really liked it.](./IMG_20190913_131058.jpg)

We had a brief bathroom break at Omotesando Hills and dropped into a
sustainable/ethical chocolate store called Imperfect. We didn't really like the
sample chocolate coated nuts that we tasted, so we didn't buy anything.

![The Roastery, Omotesando: coffee after our ice cream! I didn't like the
premium beans (630 JPY) as much this time and some of it was probably to do
with how hard it was to sip and enjoy my espresso properly from this
glass.](./IMG_20190913_135026.jpg)
![Burton, Omotesando: they have a cool room! For some reason we weren't
allowed inside.](./IMG_20190913_141538.jpg)

Having run out of things to do at about 1500, naturally we took the subway to
Ginza for more shopping.

![Nissan Crossing, Ginza: a common sight here.](./IMG_20190913_153024.jpg)
![Mitsukoshi, Ginza: briefly sitting down on the rooftop.](./IMG_20190913_153717.jpg)
![This really looks like water at first.](./IMG_20190913_154906.jpg)
![Back on top of Ginza SIX. I don't remember reading this sign before.](./IMG_20190913_164352.jpg)

In the elevator, one of us accidentally pressed the wrong floor number and Bob
pressed it again to undo/deactivate it. My mind was blown.

![Tsujiri, Ginza SIX: So delicious, and so dense and viscous that it doesn't
really seem to sag into the cone under its own weight. I'm surprised that it
even manages to come out of the machine.](./IMG_20190913_170727.jpg)
![Bob and Christine had some great tea served in a disposable traditionally
shaped bowl.](./IMG_20190913_171120.jpg)
![I hadn't seen this before either.](./IMG_20190913_172305.jpg)
![I forgot to take a picture on the day because of how amazed I was when I
opened it. I might have to hang this in my room when I get back.](./IMG_20190918_031730.jpg)
![Tsutaya Books, Ginza SIX: clocks and watches with really cool dials. A
friendly staff member came along and showed us how it worked, then showed us
a few other interesting gadgets too like a lamp in the shape of a book, which
lights up when opened. Both staff members were foreigners and spoke fluent
English, but I'm not sure if it was just a coincidence on that particular
shift.](./IMG_20190913_173150.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190913_173911x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Things which belong on my desk.
  </figcaption>
</figure>

![Held up using magnets.](./IMG_20190913_173916.jpg)
![So it turns out that these are double walled. They're so thin that I never
would've guessed. Explains their crazy price.](./IMG_20190913_174243.jpg)
![There's no control experiment to compare this to so I wasn't really that
impressed, but still not bad since this photo was taken at
1742.](./IMG_20190913_174232.jpg)
![Some other shops in Ginza SIX: more of these.](./IMG_20190913_175846.jpg)
![How will I hold my phone?](./IMG_20190913_180657.jpg)

After a really long day on our feet with barely any breaks to sit down, we
walked a decent way to Yurakucho for some skewers. I remember being dead tired
and demoralised on the walk, especially since I had worn my backpack that day
and taken my gimbal with me. I'd also made the mistake of wearing my Flyknits
rather than my Ultraboosts and my feet were a little sore. I was also really
dehydrated especially after eating all that ice cream.

![An izakaya at Yurakucho which happened to be the exact same one that I went
to with Jack and Cat last time.](./IMG_20190913_190032.jpg)
![I was so thirsty that I ordered two glasses of oolong tea one after the
other. I didn't sleep very well that night and kept waking up, so maybe this
had something to do with it?](./IMG_20190913_190039.jpg)
![We ordered chicken, pork, and tongue (not sure from what now that I think
of it) but I only took a photo of the tongue.](./IMG_20190913_191509.jpg)

The food was good although I got the feeling that the person who took our orders
wasn't being particularly friendly to us. We were also sat right next to the
kitchen and all of the smoke was blowing into our faces, making my eyes sting.
Since I was so dehydrated, I kept asking for more water and I used the
opportunity to explain myself in Japanese by saying that I hadn't drank water
all day. I later realised with the help of Elbert that I said some things
slightly wrong and due to the vagueness of Japanese, it was probably interpreted
as "I won't drink today" rather than "I didn't drink water today", which could
sound a bit offensive. Luckily I said it at the end of our meal.

![A few minutes walk from Tokyo station: we spent a while taking pictures
here. In hindsight I'm not really sure why, but it was fun. It was cool
enough outside that I put on a jacket!](./IMG_20190913_200523_1.jpg)
![It was a little bit windy so we needed to stabilise the tripod since
Christine's phone was at risk of falling.](./IMG_20190913_201405.jpg)
![Bake Cheese Tart, Tokyo station: they look so good.](./IMG_20190913_203835.jpg)
![We got three chestnut and three original cheese flavour. We ate one each
before we got on the train to Akihabara.](./IMG_20190913_204019.jpg)
![Yodobashi Camera, Akihabara: such a time sink, but that's just shopping I
guess.](./IMG_20190913_212140.jpg)

After tagging along with Bob and Christine to shop at Yodobashi Camera, we ate
the other three cheese tarts and said our goodbyes. I wasn't sure what we were
going to do tomorrow so I was a little sad since it might've been the last time
that I saw them (in Tokyo, not in general of course). I went home and waited at
Ootsuka station for a little while since Elbert had the keys. I didn't end up
blogging because I was feeling dead.

##### Waking hours

1020–2545
