---
title: Shinjuku Gyoen
date: "2019-09-25"
---

After booking sushi lunch for tomorrow, Elbert and I left the house for Shinjuku
at 1515.

![Ichiran, Shinjuku: there was no wait at all.](./IMG_20190925_154828.jpg)
![I loved the egg but it's no longer my favourite, although I don't know what
is.](./IMG_20190925_155542.jpg)
![I was still hungry so I had kaedama. When I rang the bell to ask for it,
the guy pulled out the metal plate and put it on top of the button, perhaps
for automatic metrics sake, or perhaps as a way of alerting the kitchen? When
he returned with my noodles he also came with my change on a
plate.](./IMG_20190925_161013.jpg)

After lunch it was time to see what we came for, Shinjuku Gyoen. It was already
1630 by then, a bit later than I'd hoped. Entry was 500 JPY each but it was
totally worth it, even more so if you spent a few hours there rather than the 90
minutes that we took. The sun set rather quickly once it started to get dark,
but I still got some good pictures in. We cut through a part of the park and
skipped the "French Formal Garden" but by the time we realised, the park was
closing. It's strange how a lot of places always play the same song over the
loudspeakers during closing time.

![Shinjuku Gyoen National Garden, Shinjuku.](./IMG_20190925_163237.jpg)
![Turns out I didn't really need to put on sunscreen.](./IMG_20190925_163408.jpg)
![ ](./IMG_20190925_163428.jpg) ![ ](./IMG_20190925_164623.jpg)
![One of the main attractions, the traditional garden. There were also two
tea houses nearby where you could enjoy tea and
sweets.](./IMG_20190925_164738.jpg)
![It was hard to get properly lit pictures due to the bright sky but dark
trees, but Google image processing is pretty
good.](./IMG_20190925_165140.jpg)
![Fenced off post-typhoon areas.](./IMG_20190925_165416.jpg)
![ ](./IMG_20190925_165547.jpg)
![I find the supports very obtrusive.](./IMG_20190925_170129.jpg)
![Everything is so well groomed.](./IMG_20190925_170236.jpg)
![ ](./IMG_20190925_170551.jpg) ![ ](./IMG_20190925_171151.jpg)
![Another lake.](./IMG_20190925_171340.jpg)
![The other side of the lake, from the same bridge.](./IMG_20190925_171637.jpg)
![Watch your head.](./IMG_20190925_171711.jpg) ![ ](./IMG_20190925_171853.jpg)
![This grew over the fence.](./IMG_20190925_171952.jpg)
![Not sure if this is a temporary solution or whether they'll just leave the
tree remains here to rot.](./IMG_20190925_172153.jpg)
![ ](./IMG_20190925_172229.jpg) ![ ](./IMG_20190925_172539.jpg)
![A fallen branch.](./IMG_20190925_172632.jpg)
![A really long and wide grass area. What was amazing was that it was all
green, well maintained, and weed free.](./IMG_20190925_172800.jpg)
![Mmmm, nice grass.](./IMG_20190925_173034.jpg)
![More importantly, it was bindi free! It felt amazing under my feet. I don't
know many places where you can stroll hundreds of metres on nice grass and
not have to worry about stepping on the wrong patch, or getting your feet
dirty from occasional dirt.](./IMG_20190925_173209.jpg)
![ ](./IMG_20190925_173718.jpg)
![This greenhouse was closed by the time we came to it just before 1800. Last
entry was at 1700.](./IMG_20190925_173934.jpg)
![ ](./IMG_20190925_174035.jpg) ![ ](./IMG_20190925_174138.jpg)
![ ](./IMG_20190925_174104.jpg)

We went to watch Once Upon a Time In Hollywood at the nearby cinema afterwards.
As we were waiting in line to buy tickets, one of the staff asked us if we'd
like to sign up to some loyalty/subscription service and get 1000 JPY credit
which we could use towards our movie ticket. As we went through the motions of
signing up, we discovered that we needed to give a credit card rather than just
a debit card, so we couldn't finish the process. When we got back to the ticket
counter, there were only three seats left in the theatre and none of them were
together.

We headed towards Ikebukuro to watch there instead, but not before stopping in
Tokyu Hands.

![placeholder](./IMG_20190925_191011.jpg)
![placeholder](./IMG_20190925_191227.jpg)
![placeholder](./IMG_20190925_195542.jpg)

I got trapped in the stationery section again which put us a little bit behind
on time. As we were walking out of the store towards the station, through an
adjacent high end fashion section of a department store, I found it a little
eerie how all the staff were standing at their stations, straight, still as a
statue, and facing towards the aisle. It felt like the start of one of those
horror scenes where everyone is still, then suddenly turns their heads towards
the camera.

![Ikebukuro: a picture of half of the line for a bus stop.](./IMG_20190925_202317.jpg)
![Grand Cinema Sunshine, Ikebukuro: elaborate lobby.](./IMG_20190925_203045.jpg)
![Expensive movie ticket but I didn't want to miss it by the time I got back
to Sydney. We walked in 1-5 minutes late.](./IMG_20190925_203106.jpg)

The movie was great and it definitely made me forget that I was in Tokyo, until
we left the theatre and I heard people talking in Japanese. I love being
completely immersed while watching something at the cinema. I saw some people
returning blankets in a basket, which is something that I could definitely use
at cinemas in Sydney (although I think some already have them).

![Every floor and escalator had heaps of posters of old movies. Very
cool.](./IMG_20190925_231621.jpg)
![ ](./IMG_20190925_231638.jpg)

![Fujisoba, Ikebukuro: really can't complain for 480 JPY. I had the onsen egg
in one go again.](./IMG_20190925_233247.jpg)

We got home a little past midnight.

![Home, Ikebukuro: let's see if this yoghurt is any good.](./IMG_20190926_002442.jpg)
![Sort of tastes like Yakult. Sorry, I don't really know the non branded term
for that taste.](./IMG_20190926_002510.jpg)
![Not bad, although I got some sugar on the desk.](./IMG_20190926_015225.jpg)

##### Waking hours

1340–not sure but I'm guessing 2800
