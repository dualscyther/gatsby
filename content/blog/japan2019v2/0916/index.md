---
title: Shopping in Ikebukuro
date: "2019-09-16"
---

We were woken up at 1210 by the doorbell but went back to sleep. It's actually
happened a few times so I guess door-to-door salesmen are well and alive here.

I left the house at 1545 to find the small Chinatown in Ikebukuro but since I
can't read Chinese and the Chinese shops are apparently mostly not on the ground
floor, I didn't end up figuring out where it was. Perhaps it's more of a
distributed thing rather than a single street.

![A ramen shop, Ikebukuro: can't complain.](./IMG_20190916_170158.jpg)
![Ramen eggs are usually my favourite part of the meal.](./IMG_20190916_170541.jpg)
![Recently I haven't been able to predict whether I'll be able to eat a lot
or a little. This time it was a lot, so I asked for kaedama (a noodle
refill). The waitress asked if I had a student card but I didn't bother to
bring it to Japan, so I had to pay the ~100 JPY.](./IMG_20190916_171608.jpg)

After window shopping at Labi (another electronics store), I spent something
like two hours at Bic Camera leisurely looking for gifts and picking up some
requests. When I finally checked out at closing time at 2200, just after the
attendant had scanned everything, she told me that if I liked them on Facebook I
could get a coupon to use for my purchase. Since I don't currently have a
Facebook account and it might take a few minutes to set one up, I gave up and
apologised, saying that I'd come back another day, leaving all my items there
for someone to put back on the shelves. Not too bad I suppose since I now knew
what I wanted to buy and where to find it. That night after making a Facebook
account and looking at the Bic Camera page, I realised that all I needed to do
was find their public page and show the picture containing a coupon code,
without needing to make an account.

![Yangguofu Malatang, Ikebukuro: I stumbled across some Chinese food when I
wasn't looking for it, and then realised how much I missed this particular
style. All the customers spoke Chinese, and the lady at the counter spoke to
me in Mandarin straight away. I must've looked very confused because then she
tried Japanese, and I still had no idea what she was saying. I tried to ask
in Mandarin if she spoke Cantonese but I'm not sure she even understood me.
She asked me what soup I wanted and I asked what her recommendation was and
she had a look no one had ever asked her that
before.](./IMG_20190916_224923.jpg)
![I loved the quail eggs as always. The
soup was way too peppery, so I guess when she recommended me a soup, it was
the pepper soup. I couldn't finish my noodles because I really couldn't taste
anything except pepper. It wasn't spicy or anything, but it felt like my
tastebuds were warping or something.](./IMG_20190916_225521.jpg)

![Home, Ootsuka: again, not super supple anymore, but still good.](./IMG_20190917_001100.jpg)
![There was one left in the fridge so I finished it off. My dinner wasn't
that big anyway.](./IMG_20190917_002521.jpg)

I sat on my laptop for ages and I only started blogging at 2800, so I slept
rather late.

##### Waking hours

1440–3015
