---
title: Hamarikyu Gardens
date: "2019-09-28"
---

I left for Shimbashi station at 1240 and found lunch upstairs in a building that
I'd read had a decent view of Tokyo Bay.

![Shiodome: tall buildings.](./IMG_20190928_132040.jpg)
![Notice that transparent sign at the bottom. Cool.](./IMG_20190928_132110.jpg)
![Caretta Shiodome, Shiodome: this elevator ride was awesome since you could
see the skyline appear before your eyes.](./IMG_20190928_133236.jpg)
![That's Tsukiji fish market, barren now since it moved to Toyosu.](./IMG_20190928_134001.jpg)
![And that's Hamarikyu Gardens, where I planned to go after lunch.](./IMG_20190928_134249.jpg)
![Not sure if there were better views upstairs.](./IMG_20190928_134524.jpg)

None of the food options upstairs interested me, so I ate at the one sushi
restaurant since it wasn't that expensive, and why not try more places.

![Misuji, Caretta Shiodome: ika (squid). A slightly tough but nothing too bad.](./IMG_20190928_135545.jpg)
![Kampachi (Japanese amberjack).](./IMG_20190928_135631.jpg)
![Hokkaido hotate (scallop).](./IMG_20190928_135711.jpg)
![Aji (horse mackerel). Texture and taste were wonderful.](./IMG_20190928_135810.jpg)
![Ebi (prawn).](./IMG_20190928_135858.jpg)
![Chuutoro: soft, fatty, and a less strong tuna flavour which balanced well
with the less acidic rice.](./IMG_20190928_135947.jpg)
![Ikura (salmon roe).](./IMG_20190928_140101.jpg)
![Anago (sea eel) with yuzu shavings. Sweet and melty. The yuzu was a good
addition.](./IMG_20190928_140229.jpg)
![Tamago (egg) with a lot less sugar than usual, such that the sugar is more
of an aftertaste. I liked it this way.](./IMG_20190928_140313.jpg)
![Dried gourd roll.](./IMG_20190928_140632.jpg)
![Steamed egg custard and miso soup.](./IMG_20190928_140905.jpg)
![Amai tomato with warabi mochi. The tomato was amazingly sweet once you
started chewing it.](./IMG_20190928_141915.jpg)

I'm glad that I got the eight piece set since I was comfortably full rather than
uncomfortably full as is usually the case with having ten or more pieces. It was
totally worth it for 2500 JPY.

![Shiodome: on the way to Hamarikyu Gardens.](./IMG_20190928_151225.jpg)
![Hamarikyu Gardens: it was a huge place. I always thinks it's a bit funny
that there's no jogging allowed in these gardens.](./IMG_20190928_153017.jpg)
![ ](./IMG_20190928_153720.jpg) ![ ](./IMG_20190928_153835.jpg)
![ ](./IMG_20190928_153911.jpg)
![The cover all comes from the one tree. That's a teahouse behind.](./IMG_20190928_154226.jpg)
![Otsusai-bashi, the main attraction at this garden.](./IMG_20190928_154338.jpg)
![ ](./IMG_20190928_154434.jpg) ![ ](./IMG_20190928_154503.jpg)
![ ](./IMG_20190928_154541.jpg) ![ ](./IMG_20190928_154553.jpg)
![ ](./IMG_20190928_154913.jpg) ![ ](./IMG_20190928_155115.jpg)
![ ](./IMG_20190928_155142.jpg)
![Three bridges connect to this teahouse island.](./IMG_20190928_155520.jpg)
![ ](./IMG_20190928_160312.jpg) ![ ](./IMG_20190928_160614.jpg)
![View from the hill.](./IMG_20190928_160852.jpg)
![I guess they built this hill and chopped down whatever branches were in the way.](./IMG_20190928_160719.jpg)
![ ](./IMG_20190928_161114.jpg)
![A nice spot to relax.](./IMG_20190928_161255.jpg)
![Plenty of benches along the canal here too.](./IMG_20190928_161345.jpg)
![Another lake, with another bridge.](./IMG_20190928_161848.jpg)
![ ](./IMG_20190928_161859.jpg)
![The translation wasn't very good, but duck catching or something.](./IMG_20190928_162145.jpg)
![Duck would be lured here and people could peek at them through this hole,
before jumping them with a net.](./IMG_20190928_162208.jpg)
![A sluice gate to modulate the water levels. Interesting that the ponds hold
salt water.](./IMG_20190928_162607.jpg)
![This was a path on the map, but it looked narrow and I wouldn't want to
walk into a spider web either.](./IMG_20190928_163209.jpg)
![A former shrine, apparently.](./IMG_20190928_163457.jpg)
![ ](./IMG_20190928_163600.jpg)
![This was the flower garden and I was a bit disappointed by it. Perhaps I
came in the wrong season.](./IMG_20190928_163718.jpg)
![ ](./IMG_20190928_163847.jpg)
![A nice grassy field behind.](./IMG_20190928_163907.jpg)
![The peony garden, which was.. nothing.](./IMG_20190928_163926.jpg)
![I wonder what made this patch? Probably not a cricket pitch.](./IMG_20190928_163934.jpg)
![ ](./IMG_20190928_163953.jpg)
![The water looked sort of stagnant here and I made my way across as quickly
as possible because I seem to be a mosquito
magnet.](./IMG_20190928_164136.jpg)
![A nice rest area.](./IMG_20190928_164312.jpg)
![This canal was used to transport goods more easily, not as a moat. I think
that's what I read anyway.](./IMG_20190928_164508.jpg)
![ ](./IMG_20190928_164803.jpg) ![Sugoi.](./IMG_20190928_164906.jpg)
![ ](./IMG_20190928_164926.jpg) ![ ](./IMG_20190928_164950.jpg)
![Just a cool looking tree.](./IMG_20190928_165234.jpg)
![And another.](./IMG_20190928_165534.jpg)
![And another.](./IMG_20190928_165559.jpg)
![Crossing a canal on the way out.](./IMG_20190928_170003.jpg)

I had nothing better to do so I walked twenty minutes to a coffee shop that I'd
read about.

![Tsukijigawa Kameibashi Park, Tsukiji: just a little park that I walked past
on the way which looked interesting.](./IMG_20190928_172538.jpg)
![A public foot massager? I couldn't read the signs so I wasn't sure what the
rules were, otherwise I might have tried it for
fun.](./IMG_20190928_172857.jpg)
![ ](./IMG_20190928_173119.jpg)
![Bongen Coffee, Ginza: I'm a sucker for these tiny shops.](./IMG_20190928_173218.jpg)
![ ](./IMG_20190928_173226.jpg)
![Medium roast Kenya hand drip was good but I would've preferred a lighter
roast.](./IMG_20190928_174318.jpg)
![I forgot to take a picture of the whole shop but here's a picture of the
card. I've been collecting cards as souvenirs to either stash or display when
I get back to Sydney. That bonsai was behind the barista.](./IMG_20190929_022935.jpg)

One thing I've noticed is that I always seem to confuse people when I'm walking
through crowds, and I can't seem to read which way they're going either. Perhaps
the unspoken norms are different here, i.e. the subtle body cues which tell
people which way they're walking and how to dodge them. I think I'd get through
a crowd a lot faster in Sydney.

![Nissan Crossing, Ginza: here again.](./IMG_20190928_195056.jpg)

I walked into a Loft and got sucked in again by the coffee section.

![Tabio, Ginza SIX: The Tabio in Ginza was huge! This is a small fraction of
what was available. This particular line of women's wool socks had so many
colours.](./IMG_20190928_200403.jpg)

![FamilyMart, Ootsuka: they have Imabari towels here.](./IMG_20190928_214654.jpg)
![Butayama, Ootsuka: I found out later from Elbert that this is called jiro
ramen. Five toppings, and incredibly heavy. I couldn't finish
it.](./IMG_20190928_220127.jpg)

I got home at 2245 after eating, and four hours later I still felt full. I don't
think I'll be eating jiro ramen again.

##### Waking hours

1120–2900
