---
title: More of The Roastery
date: "2019-08-21"
---

I wanted to try out the public cupping that The Roastery held every Wednesday
for a few reasons:

- just for the experience since I've only tried coffee cupping once before.
- see what it was like in Japan, and at this place.
- as an excuse to drink more coffee there.
- because why not, let's do something random that I didn't really expect to be
  doing.

I roped Elbert into coming along and unsurpringly, we got out of the house
pretty late at 1645 (I spent some time learning Japanese and looking at a shoe
sale in Australia[^1]); we quickly ate some ramen in Harajuku to make it there
by 1800 since I had no idea whether it was going to be a proper, organised event
or whether it was a very casual, come whenever sort of thing.

![Harajuku somewhere: my favourite thing about this was getting veggies,
although they're buried in this picture. The left and bottom are pork.](./IMG_20190821_173433.jpg)
![Elbert's ramen, with chashu.](./IMG_20190821_173437.jpg)
![Plenty of toppings to add. The ramen is "ore" (me) style, i.e. you add stuff
to suit yourself.](./IMG_20190821_175514.jpg)
![A description of the free toppings: seaweed, kelp, yuzu pepper paste, plum
paste, burnt garlic paste. Also check out the fans that I assume are there for
you to use if you're feeling hot.](./IMG_20190821_175519.jpg)

It turns out that the cupping was very casual and the beans had already been
brewing in the water for twenty minutes by the time that we got there. You just
walk in, pick up a spoon, and do your thing[^2]. Elbert was hesitant to try it
at first but I managed to nudge him into it, with some help from the friendly
lady staffing the cupping counter, who both convinced him and explained to him
the cupping process.

![The Roastery, Omotesando: you get to cup all of their beans that they have for
sale.](./IMG_20190821_183557.jpg)

Being pretty inexperienced, I failed to find many adjectives to describe how
each of the beans tasted, but I still found a clear favourite. Unfortunately, it
turned out to be the most expensive one, so I would have to put a hole in my
wallet today. I decided to get the beans as a pourover (I don't think they were
serving it as espresso anyway). For their regular beans this would cost 650 JPY,
but mine cost 1000 JPY.

Elbert was asked if he wanted something too, which he again wasn't too sure
about. Coincidentally, I had a page open on my phone which taught me how to say
something along the lines of "it's my treat tonight" in Japanese, so I pulled it
out and recited from it, getting a "wow so kind" in jest from the lady, in
English. Elbert was probably so peer pressured and so embarassed by my poor
Japanese that he said yes. The lady was super kind; Elbert told me that she had
asked whether he drank coffee and that he had said no, so she recommended that
he try the cold brew, and then later checked up on him to offer syrup and ask if
everything was okay, seeming somewhat worried about him.

![My pourover being... poured over. The filter is reusable and there isn't
really a cone to hold it.](./IMG_20190821_182407.jpg)
![1000 JPY coffee. Really tasty, really clear flavours. My non espresso palette
is pretty undeveloped so I can't say that it was worth it for me personally, but
yes it was very good. I forgot to take a picture of Elbert's 800 JPY cold brew,
but it was also great. We both preferred mine though.](./IMG_20190821_182733.jpg)
![Sitting in the exact same spot as three days ago.](./IMG_20190821_183539.jpg)

I'd been wanting to get my own pair of Birkenstocks since I'd worn Michelle's
and found them to be comfortable. I walked into the shop in Harajuku just to
have a browse, but a staff member followed me upstairs, asked to measure my foot
size, and asked if I wanted to try x and y sandals on. I didn't refuse since
this was pretty much why I'd come to the store after all. Luckily for me, I
didn't need to rebuff his advances since the two pairs that I was considering
were both on sale for 8400 JPY–neither cheap nor expensive–but Michelle's
weren't a perfect fit and buying some in Tokyo would be good so that I wouldn't
need to wear hers.

I had to choose between a more supportive style with a higher arch and a spot
specifically for the big toe, or one with no big toe spot and a lower arch,
which would be more comfortable but not as good for a long day of walking or for
running for the train. I ended up choosing the big toe-less style after I was
reminded that you could wear socks with them–an uncommon use case, but a use
case nonetheless.

My friend Ashley had asked me to buy him something from The North Face so I had
a look around, but left empty handed. It was pretty strange because it was split
into four stores, all literally next to each other, but not physically connected
as far as I could tell. I had to go into all four to check since the staff
weren't sure[^3] whether the other stores had what I wanted.

We walked to Shibuya and ended up deciding to play pool. The lift up to the
pool/darts place was next to a Uniqlo, so naturally I went in to have a look
first before we went up.

![Next to Uniqlo, Shibuya: spotted this potential dinner location at the lift as
we went to play pool.](./IMG_20190821_203616.jpg)
![Elbert about to stomp me in a game. You can order using the phones on the
wall even though reception is pretty close.](./IMG_20190821_203946.jpg)
![Ginger ale and melon soda, since there was a minimum order of one drink per
person.](./IMG_20190821_211553.jpg)

After playing for two hours, we were hungry for dinner and Hooters happened to
be right upstairs, so we had a look because why not. Unfortunately their kitchen
was closed so we did the next best thing and went to Maccas.

![McDonalds, Shibuya: 10 nuggets and large fries for 500 JPY. They don't do
sweet and sour sauce but they do have fruit curry, barbeque, ketchup, mustard.
The chips were stale and I'm never bothered/ballsy enough to ask for better
chips in Sydney, so I'm definitely not going to do it in Tokyo where I can't
speak the language.](./IMG_20190821_231823.jpg)
![The fruit curry sauce tasted like Japanese curry. The nuggets weren't
"straight off the heat" fresh, which is usually the only way that I like them in
Sydney, but they were actually still good. Maybe they have a better method of
keeping their nuggets tasting good (same with the hot chicken at conbini
stores), or maybe I was just hungry.](./IMG_20190821_231909.jpg)
![Home, Ootsuka: they wrapped the bag in plastic to protect it from the rain,
which was nice, but I wish they would've asked first (although I probably
wouldn't be able to understand).](./IMG_20190822_005055.jpg)
![This is what I bought, since I know that I failed to communicate effectively
above.](./IMG_20190822_005343.jpg)
![Tomatoes which I bought from yesterday. No-brainer since they were so good the
day before. I also bought a 900mL milk tea carton on the way home,
but forgot to take a picture.](./IMG_20190822_023110.jpg)

We got home at 2440 so I had a pretty late start on my daily post. It didn't
actually take too long so I don't really know why we slept so late, where does
the time go!?

##### Waking hours

1515–3030

[^1]:

  Adidas were having a 50% off sale so I actually wanted to go and try them on
  in person first at the store in Harajuku, but I ended up forgetting about it.

[^2]:

  In my limited experience: you smell the coffee, slurp a spoonful, (optionally)
  spit it out into a cup, then dip your spoon into water to clean it. Rinse and
  repeat with every coffee, however many times you like. The coffee is brewed in
  a standard (and not tasty) manner so as not to influence the taste.

[^3]:

  They were mostly sure by checking their own website, but how many retail store
  websites have you seen where you can find what you want easily? Especially if
  you don't know exactly what you're looking for.
