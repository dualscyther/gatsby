---
title: Day 0 in Japan
date: "2019-08-09"
---

After my three week trip to Japan in Februrary, I knew that I was going to come
back. I loved walking around the streets of Tokyo and just taking it all in.
When I decided to take six months off from working until I started my grad
position at Atlassian, it was the perfect opportunity for me to go back.

I don't mind sightseeing and doing typical touristy things, but my favourite
thing to do is to relax, do things at your own pace, and take time to get a feel
for and appreciate the local culture. With that in mind, ten weeks seemed like
an appropriate amount of time to spend in Japan. Luckily for me, my friend
Elbert, who wants to move to Japan permanently at some point, was going to rent
an apartment in Tokyo starting from June, so accommodation was sorted.

The next few months of regular (hopefully daily) blog posts will detail my
activities in Japan. They will somewhat be word vomit style, and only very
lightly edited. Maybe I can also do vlogs but who knows.

---

I started packing for my 6am flight pretty late, which meant that I only got a
single hour of sleep. On the plane, the menu was written in both English and
Japanese, so I tried to read the Japanese half since I had recently learned
Hiragana and Katakana (Kanji will take me years, but I'm up for the challenge).
I spent the flight sleeping, learning Japanese on LingoDeer, and watching a
subtitled Japanese movie (Masquerade Hotel) on another passenger's screen,
through the gap between the two seats in front of me (I don't really know why, I
guess I was just sort of lazy).

I arrived at Narita airport and took an all stops train on the way to Ōtsuka
station rather than catch the half an hour shorter limited stops, which costs
1500 JPY more. My time is worth less than \$40/h at the moment ;). That night I
had Chūka soba (中華そば) (Chinese soba, which Elbert tells me is essentially
just a synonym for ramen). The Tokyo summer humidity makes it somewhat
uncomfortable at night, which made me pretty apprehensive about what it would be
like the next day. The apartment itself is not too shabby at all, with the main
room having AC and space for a futon, a bed (Elbert gave it up for me to use,
against my protests), a monitor, a desk, and sitting room for the both of us.
What more do you really need?

![Hopeken, Ootsuka: Chūka soba](./IMG_20190809_231652-chuukasoba.jpg)
