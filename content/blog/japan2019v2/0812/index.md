---
title: Comiket
date: "2019-08-12"
---

Elbert and I wanted to go to Comiket—one of the world's biggest fan conventions,
focusing on publishing amateur work—just to check it out, but naturally we got
out of the house pretty late at 1330, even though it closed at 1600. Luckily,
this also meant that we didn't need to line up or pay the small fee for entry,
since it was beginning to empty as we arrived.
[This is a good summary of Comiket with an associated gif of how insane the crowds are](https://np.reddit.com/r/gifs/comments/3yt9qb/crowd_control_at_comicbook_convention_in_japan/cygk80q/).

Having never been to an anime/manga/comic/whatever convention, and having not
done any reading about Comiket beforehand, I was caught entirely offguard by
what I saw. There were a few large halls where all the stalls were located; the
two that we visited were purely filled with adult fan art and cosplay modelling.
It was surreal seeing how business-as-usual everything was; most stalls had
large provocative posters stood up, matching the covers of the
books/magazines/albums on sale. Thousands of patrons walked the rows of stalls,
like a busy fresh food market. Just like the fan art stalls were staff by their
own artists, so too were the cosplay stalls mostly staffed by their own models.

Unfortunately, we didn't catch any particularly extravagant or amazing
cosplayers walking around—they may have been taking pictures outside, or we came
on the wrong day or at the wrong time. It was still a worthwhile experience to
see how Japan can be progressive in some ways yet quite traditional in others.
The perspective amongst the patrons at Comiket is that art is still art
regardless of genre and should be respected in its own right.

![Comiket, Tokyo Big Sight, Odaiba: my only picture at Comiket since I spent
most of the time holding my gimbal. This picture doesn't really capture the
experience or how busy it was. The poster behind me would rank amongst the less
titillating things that I saw that day.](./IMG_20190812_154806.jpg)

I wanted to do some shopping for climate appropriate t-shirts and a small bag
for my action camera, so we headed to Ginza to shop at Uniqlo's twelve floor
flagship store. I had forgotten how nice it was around the area, and how they
close road traffic on the main strip in the afternoon on weekends and public
holidays (today was "Mountain Day"). Some combination of the buildings blocking
the sun and copious amounts of cold air coming out of shops actually made it
pretty bearable to walk around and just enjoy the view and ambiance. The Uniqlo
itself was as cool as I remembered, and I probably spent an hour or two in
there. Some of the collaboration t-shirts were amazing, including a really cute
Hello Kitty one (I'm not even into Hello Kitty whatsoever) and a Pocky one which
unfortunately was out of the sizes that I wanted to buy.

![Nice view down Ginza's main street. The colour of the road was noticeably
unique too, which made it nicer.](./IMG_20190812_171101.jpg)
![Uniqlo, Ginza: Your Name t-shirt.](./IMG_20190812_190845.jpg)
![The Attack on Titan shirt looked cool. Too bad the anime sucks.](./IMG_20190812_191021.jpg)
![Blizzard collaborations that I didn't know existed. I don't actually play
Overwatch though.](./IMG_20190812_191058.jpg)
![Hearthstone! Pretty nice shirt.](./IMG_20190812_191102.jpg)

![7-Eleven, Ginza: we stopped here on the walk to Ginza from Shimbashi station
since, you know, it was pretty hot. I was wondering if this ice cream daifuku
was going to be as tasty as I remembered from last trip.](./IMG_20190812_165353.jpg)
![Yes it was. I don't think I can get sick of eating these.](./IMG_20190812_165648.jpg)

Since we were in the area, we tried to find Sukiyabashi Jiro from
[Jiro Dreams of Sushi](https://en.wikipedia.org/wiki/Jiro_Dreams_of_Sushi) just
so that I could take a photo. We went down a set of stairs towards the subway
(which I had been down before in February for the same reason) but then somehow
got lost for ten minutes, but then eventually figured out that that particular
underground section containing the restaurant was closed off 😑.

After eating dinner at a restaurant inside Tokyu (sic) Plaza[^1], we went home
and got back just before 11. I unsuccessfully attempted to write two blog posts
in one night, as well as learning some kanji.

![Ginza Sony Park: Me clutching my new Uniqlo purchases next to an aquarium in
a park that we happened to walk past.](./IMG_20190812_200514.jpg)
![Ginza Torisuki, Tokyu Plaza: a tiny fig and chicken side. At least the
fig was tasty.](./IMG_20190812_204222.jpg)
![Soba with grated radish and plum, which added nice acidity. Elbert had what
was essentially a yakitori don.](./IMG_20190812_205255.jpg)
![Matcha tea served with warabimochi, tea ceremony-ish style.](./IMG_20190812_211949.jpg)

I also found out
[that one of the plugins that I'm using](https://www.gatsbyjs.org/packages/gatsby-remark-images/)
to build this site automatically optimises images so I can just upload and link
the full 5MB image in my content and it will generate a significantly compressed
(<1MB, but configurable) image for the actual page. Yay!

##### Waking hours

1200–2900

[^1]: one of the dozens (hundreds?) of big malls in Ginza
