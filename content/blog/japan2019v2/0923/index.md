---
title: How I've been organising my stay and keeping track of my spending
date: "2019-09-23"
---

![I've been using Trello to keep ideas for things to do. You can also see
some ideas in the "Blog" column that I haven't gotten around to writing about
yet. I've been using Trello to keep notes about each day since it syncs with
my phone; it's not really made for it but it
works.](./Screenshot-from-2019-09-26-02-59-27.png)

![I've been using MoneyWallet to keep track of expenses. It has features that
I'm not using like keeping track of income and having multiple "wallets". I
chose it primarily because it's FOSS and you can download it from F-Droid. It
also has a handy "export to csv" feature which I plan to use later. I would
say that it lacks polish here and there, but it does the
job.](./signal-attachment-2019-09-26-030532_002.png)
![You can go into quite a bit of detail with each item. I've opted to
actually add these details so that I can analyse my spending if I'm ever
bored.](./signal-attachment-2019-09-26-030532_001.png)

## Autumnal Equinox Day

A public holiday where I did nothing...

When I woke up I planned out my Hokkaido trip for a bit, and looked up sushi
places to try to book. I think I've left both of those a little late. Elbert
called a few of the places that I'd chosen but none of them picked up. When we
went out to eat we realised that there was a public holiday on, which is
probably why we weren't able to book.

![Home, Ootsuka: pre breakfast snack.](./IMG_20190923_155013.jpg)
![OMO5, Ootsuka: looking around for food, we went up to a hotel cafe, but the
kitchen was closed since it was 1700. The decor outside the lifts was awesome
though.](./IMG_20190923_170319.jpg)
![Useful for hotel guests.](./IMG_20190923_170326.jpg)

We ended up back at Eightdays Cafe and they had a special offer where you could
have all you can eat Italian ham for an hour for 1000 JPY, as long as you
ordered a drink too. The waitress told Elbert that he obviously wouldn't be
allowed to eat from my plate (since he wasn't going to order it) but it was
otherwise fine, but when she went to the kitchen with our order, she came back
and said that it wasn't possible for only me to order the all you can eat meal.
Annoying.

![Eightdays Cafe, Ootsuka: chicken curry. I haven't eaten baby corn in a
while.](./IMG_20190923_173658.jpg)

I fell asleep on my bed after lunch, then started writing my second blog post of
the day while eating jellies and gummies.

![Home, Ootsuka.](./IMG_20190923_202553.jpg)
![Not a fan.](./IMG_20190923_211001.jpg)
![These were like starbursts but better. Although I wish that each piece was
larger.](./IMG_20190923_224104.jpg)

Elbert and I ate ramen in Ootsuka at 2430. The noodle shops being run by one
person always impress me. Even though she seemed so busy she still had time to
leave the kitchen to help two foreigners understand the menu on the machine when
they walked in, which was really nice, but I guess that's how you get
customers..

![A ramen shop, Ootsuka: Shio-tonkotsu Yokohama Ramen, which was delicious,
but I prefer thinner noodles.](./IMG_20190924_003029.jpg)

On the way back home I pulled out my phone and had no WiFi, which made me
realise that I'd left my bag at the restaurant. Before walking back in, I looked
up the word for "to forget" so that I would be able to explain myself.

![Ootsuka: I noticed that this fence which prevents access to the railway
overpass is covered in really scary looking
spikes.](./IMG_20190924_005249.jpg)
![At 7-Eleven, I saw this and read that it had a white chocolate coating, so
I gave it a pass and went looking for pudding. When I couldn't find any I
went back and bought this, completely forgetting that it had white chocolate
on the outside. I only realised my mistake when I was unwrapping it. Far too
sweet, as I imagined.](./IMG_20190924_011400.jpg)

I finished my second blog post and did half of a third one before I slept.

##### Waking hours

1310–2740
