---
title: Roadtripping day 4
date: "2019-09-12"
---

Bob and I got up early to go for a 4-5km run around part of the lake. The sky
was clear and we could see Mount Fuji really well, although we should've ran
counter-clockwise instead because there was a section that had a great
unobstructed view but we were facing in the wrong direction. I hadn't gone for a
run in over a year so it was nice, and the air was cool. The sun came out over
the mountains just as we started running so we probably could've timed it half
an hour earlier.

Breakfast at 730. Christine ate half of her food so we ate most of the rest of
it.

![Rakuyu, Fujikawaguchiko: kaiseki breakfast at 0730.](./IMG_20190912_073203.jpg)
![I think the many different small dishes makes it easy for me to finish even
though it's a pretty large breakfast. Christine was a bit sick of kaiseki at
this stage and Bob and I had to help her finish
everything.](./IMG_20190912_073222.jpg)
![ ](./IMG_20190912_073747.jpg) ![ ](./IMG_20190912_073758.jpg)

We regretted having breakfast so early and Bob and I went back to sleep until it
was time to check out. The temperature was in the low twenties today which is
the coolest it's been since I arrived in Japan. It made the day very pleasant.

![Checking out the view while we were checking out.](./IMG_20190912_082252.jpg)

![Cheesecake Grande, Fujikawaguchiko: grabbing some morning tea at the bottom
of the hill that our hotel sits on.](./IMG_20190912_102432.jpg)
![Fujiyama unbaked cheesecake. Great cake, unsurprisingly bad coffee.](./IMG_20190912_102945.jpg)
![Aokigahara Forest: also known as the suicide forest. A huge forest with an
incredible amount of densely spaced, old, tall trees. We drove through on the
way to our next stop.](./IMG_20190912_110520.jpg)
![Idebok Cow resort, Fujinomiya: we spotted this and couldn't resist stopping
to take a look.](./IMG_20190912_112922.jpg)
![Nothing particularly interesting to be honest.](./IMG_20190912_113613.jpg)

We drove to Fuji Milk Land which sounded really cool, but just had some farm
animals, a restaurant, and a souvenir/food shop. I took pictures of all the
animals but they weren't really interesting so they're not shown here. There
weren't many other visitors present either. One cool thing we saw was a class of
schoolchildren taking out individual picnic blankets to eat their lunch, and one
of them had one printed to look like a ramen menu.

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190912_115646x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Fuji Milk Land, Fujinomiya: best keyring that I've seen yet.
  </figcaption>
</figure>

![A really fat cow that was just standing there and eating hay. I think you
can pay to milk it.](./IMG_20190912_120758.jpg)
![ ](./IMG_20190912_120827.jpg) ![Looking very sad.](./IMG_20190912_121155.jpg)
![If there's something that I've learned it's that Christine really likes
animals.](./IMG_20190912_121314.jpg)

Then it was off to see some waterfalls. Unlike all the other parking in the
countryside, we actually had to pay 200 JPY to park. There were actually quite a
few private carparks trying to hustle cars into their spaces, all charging
different rates. Maybe it was shops with some extra land trying to make a bit of
extra money.

![Shiraito Falls, Fujinomiya: technically this is the top of Otodome Falls, a
smaller waterfall a five minute walk away from Shiraito
Falls.](./IMG_20190912_124314.jpg)
![ ](./IMG_20190912_124657.jpg)
![Little wasabi plants.](./IMG_20190912_124810.jpg)
![Shiraito Falls. You can't see it from here but there are lots of smaller
waterfalls all down the right side of the river.](./IMG_20190912_124952.jpg)
![ ](./IMG_20190912_130635.jpg) ![ ](./IMG_20190912_131054.jpg)
![A nice picture with autumn leaves and Mount Fuji in the background.](./IMG_20190912_131614.jpg)
![I didn't take that picture though.](./IMG_20190912_131609.jpg)

On the way back to Odawara Station we refuelled before we returned our rental
car. I'm not sure if it was just a quiet period, but they had a three person
team service us at the petrol station. I'm not sure who did what, but they told
us exactly when to stop, took payment through the window, and pumped our gas.

After returning the car, we looked around the station for some food to eat.
While I was waiting for Bob and Christine to get their food, a middle aged
Japanese lady began to talk to me. She must've seen me browsing my phone in
English because she asked me a series of questions in English and I had no idea
what was going on so I just answered her, trying to do my best to speak
Japanese. What was stranger was that she was writing her questions in English on
the back of an envelope as she said them. Some questions that I remember are:

- are you alone? (this was the creepiest one)
- where are you from?
- what time is your train?

Bob and Christine came back and we left to go to the platform. It was only then
that we hypothesised that she was probably just practising her English. I wish
she would've asked first because then I would've actually loved it, rather than
being weirded out by the strange questions (which we realised sounded like
something straight out of a textbook).

![We boarded the train, then realised that it would take about an hour and we
wanted to eat our freshly bought takoyaki, so we got back off to eat it and
then caught the next one.](./IMG_20190912_161414.jpg)

Halfway through our train ride we came to a slightly abrupt stop and an
automated announcement came on (in Japanese, Mandarin, and English) saying
something like "the emergency brake has been engaged". The driver made an
announcement in Japanese but we had no idea what was going on, although we saw
lots of people make phone calls and a few groups of Japanese people talking
amongst themselves, most likely figuring out what to do about being delayed.
Eventually an automated announcement came on saying that there had been a human
related accident ahead of us. After what seemed like 10-20 minutes our train
continued onwards. When we went past the next station we saw a train stopped
with staff looking down into the gap between the train and the platform.

Bob had an aunt who lived in Tokyo and had planned to have dinner with us that
night. She picked us up from the station in a 7-seater van and watching her
drive on Tokyo city roads was absolutely crazy and scared me off of ever driving
here. We parked underneath a shopping centre and there were another 3-5 people
who serviced us. Bob's aunt said that it was partly because companies are
subsidised by the government to hire the elderly.

![A yakiniku restaurant, Nihombashi. Apparently this place is famous because
it buys entire cows rather than individual cuts of
meat.](./IMG_20190912_182304.jpg)
![Cooking some meat sushi.](./IMG_20190912_182706.jpg)
![So oishii.](./IMG_20190912_182801.jpg)
![Each cut is numbered and the letters tell you which sauce is recommended.](./IMG_20190912_183437.jpg)
![This tells you which piece of beef comes from where.](./IMG_20190912_183616.jpg)
![Tongue. This is the first time that I've had tongue cut thickly rather than
thinly. I rather liked it.](./IMG_20190912_185835.jpg)
![Grinding salt onto another plate of meat that Bob's aunt sneakily ordered.
Asian hospitality...](./IMG_20190912_190638.jpg)

Bob's aunt was really friendly and a great host. She was tri/quadlingual (fluent
in Mandarin, Shanghainese, Japanese, and good enough with her English that we
could have a conversation). I'm not sure if it's because she's from a Chinese
background, she's a confident older adult, or she was hosting us and wanted to
make sure that we would enjoy the food, but when she ordered, she had a very
lively discussion with the waiter about the menu in a way that I haven't seen
other Japanese people do. I had fun practising my Japanese with her.

After dinner, she took us around Nihombashi–an older business district with lots
of company headquarters, and expensive stores–for some sightseeing and a little
bit of window shopping.

![Nihombashi: the first Mitsukoshi store.](./IMG_20190912_193822.jpg)
![A pastry shop, Nihombashi: apparently these are edible but not for sale.
Bob's aunt bought some pastries for Bob and Christine, which they ate back at
their hotel room. Apparently they were delicious so I might come back,
especially because they're Japanese flavoured desserts (like red bean) rather
than western style.](./IMG_20190912_194336.jpg)
![Touching the point which marks the centre of Tokyo, used for distance
measurements.](./IMG_20190912_195327.jpg)
![I don't remember what sort of shop this was, but it might've been food.
They had a small gold plated room which you could go
into.](./IMG_20190912_200200.jpg)
![ ](./IMG_20190912_200142.jpg)
![A tea shop with a million different varieties. Like T2 but way cooler.](./IMG_20190912_201332.jpg)
![The pineapple tea really tasted like pineapple.](./IMG_20190912_201600.jpg)

We got back into the car and drove to Bob's aunt's parents place, where she
picked up her two kids. Christine and I tried asking them in Japanese what their
names were, but they couldn't understand us and one of them said with a really
confused look, "nani?" but then Bob spoke to them in Mandarin and they
understood.

Bob's aunt dropped me off at Akihabara station and I spent some time in
Yodobashi Camera since it was right next to the station, then went home and
tried to blog but didn't finish since I was dead tired.

![Home, Ootsuka: eating a pear which tastes a bit less supple after a week.](./IMG_20190912_230834.jpg)

##### Waking hours

0600–2610
