---
title: Nezu Shrine
date: "2019-09-05"
---

I left the house at 1600 to visit Nezu Shrine. The temperature felt nice walking
to Sugamo station, but when I got off a few suburbs over, it was humid. I didn't
realise that there could be such localised differences.

![Nezu Shrine, Nezu: the western entrance.](./IMG_20190905_163625.jpg)
![One end of the torii path that stretches along the side of the area.](./IMG_20190905_163724.jpg)
![A platform overlooking a pond with koi and turtles.](./IMG_20190905_163954.jpg)
![The gates built quite unevenly. Some are slanted, some are wider, some
are narrower, and some are shorter. Even I had to duck under many of
these gates.](./IMG_20190905_164212.jpg)
![The other end of the torii path.](./IMG_20190905_164257.jpg)
![The garden was closed off but this minimal gate was interesting.](./IMG_20190905_164303.jpg)
![Looking up at where I was looking down from before.](./IMG_20190905_165224.jpg)

![I came from the back left of the picture, but I came here to take a picture
from the front. Just to the left of the frame were some gardeners toiling in the
humidity.](./IMG_20190905_164426.jpg)
![I thought that this gate was quite beautiful.](./IMG_20190905_164443.jpg)
![I'm not sure what this functions as but it was across from the purification
fountain.](./IMG_20190905_164522.jpg)
![Oh crap there's no sign telling you what to do, I better remember.](./IMG_20190905_164526.jpg)
![So glassy. It was even better before I washed.](./IMG_20190905_164634.jpg)
![The main building.](./IMG_20190905_164816.jpg)
![Same thing but from further away.](./IMG_20190905_164837.jpg)
![Wishing plaques.](./IMG_20190905_164852.jpg)
![Fortunes. Do they remove these at the end of the day?](./IMG_20190905_165049.jpg)
![Loving this map.](./IMG_20190905_165904.jpg)
![The view from near the front entrance. You can see the bridge, the torii
path, the big gate, the wall/gate behind it, and the temple
itself.](./IMG_20190905_165946.jpg)
![The front entrance.](./IMG_20190905_170257.jpg)

I was about twenty minutes from Ueno so I walked down to find some food and find
something to do.

![Some small sweet potato store on the way to Ueno: "Purple fleshed sweet
potato" soft serve. I liked it.](./IMG_20190905_172153.jpg)
![Some weird half solid, sludgy thing made from sweet potato. A lot more
filling than it looks.](./IMG_20190905_173132.jpg)
![The western perimeter of Ueno Park. Noticeably more humid here too than the
rest of the walk. Not sure why. I also earned another mosquito bite
here.](./IMG_20190905_174524.jpg)

When I got to Ueno I walked around the street market area with lots of cheap
goods and street stalls selling food. I noticed that a lot of the food stores
lining the streets were Chinese shops. I walked past a few kebab stores which
definitely put me in the mood for kebabs. As I was about to pass by another one,
the Turkish guy standing out the front did the usual thing and tried to get the
couple in front of me to eat food from his shop, except that he recognised that
they were Chinese and spoke Mandarin to them. This made me afraid that he was
going to speak Chinese to me but instead he spoke to me in English! How did he
know? Since I was keen for a kebab anyway, I let him strike up a conversation
and convince me to eat at his stall. He asked me where I was from and I said
Australia, to which he immediately replied "ahh Aussie, OI OI OI!" Is this
something that Australians are known to obnoxiously do even overseas?

I talked to him a little bit later as I was eating a kebab from his shop, and he
said that he'd been in Japan for ten years and that he could speak Japanese,
Malay, Mandarin, English, and Turkish of course. I realised that he probably
wasn't fluent though since his English wasn't great except for the stuff that he
knew how to say to get me into his shop. He said that he'd previously lived in
Malaysia, Taiwan, and China (I think) too. It was especially amusing watching
him when Japanese girls came past; I think he made jokes or paid them
compliments to get them to laugh and talk back to him.

![Ameyoko, Ueno: chicken kebab (700 JPY). Tasty and a nice change from the usual
food that I've been eating recently.](./IMG_20190905_184147.jpg)
![Ameyoko, Ueno: green tea with grass jelly. I've never had it with a lid
like this. They also offered to put the straw in for me which was nice. Too
bad three quarters of the grass jelly was way too big for the straw so I
couldn't suck it up. So silly.](./IMG_20190905_190844.jpg)
![Home, Ootsuka: either I'm getting sick of these, they got worse in the
freezer since I opened them, or these smaller ones aren't as good as the ones
from the conbini. Pretty eh.](./IMG_20190906_003632.jpg)
![I bought quite a few of these from the Mega Don Quijote. Not bad at all.](./IMG_20190906_021648.jpg)

My friends Bob and Christine were arriving early the next morning and I was to
meet up with them in Yokohama, but that didn't stop me from sleeping a bit later
than I wanted to 😔.

##### Waking hours

1455–2800
