---
title: Roadtripping day 3
date: "2019-09-11"
---

![Rakuyu, Fujikawaguchiko: morning view from our room.](./IMG_20190911_080243.jpg)
![Kaiseki breakfast nice and early at 0800. Seeing and tasting so many
different dishes is a little overwhelming, but also cute and
fun.](./IMG_20190911_080628.jpg)
![Some sort of bean paste ball.](./IMG_20190911_080634.jpg)
![ ](./IMG_20190911_083405.jpg)

We got ready and had a short nap after the huge breakfast, then left at 1040.

![Arakurayama Sengen Park, Fujiyoshida.](./IMG_20190911_105159.jpg)
![I had no idea where we were going and it turns out that Bob had been here
before. My legs weren't ready for 398 steps to get to the pagoda at the
top.](./IMG_20190911_105820.jpg)
![This is the view that you're supposed to see at the top.](./IMG_20190911_110605.jpg)
![Chureito Pagoda, Fujiyoshida: unfortunately Mount Fuji was obscured at the
time.](./IMG_20190911_111212.jpg)
![All those houses built on the plains.](./IMG_20190911_112134.jpg)
![We saw a ritual being performed on a car and speculated what purpose it was
for.](./IMG_20190911_113553.jpg)
![We prayed here on the way down since we had exactly three spare 1 JPY
coins. I also witnessed a woman running with dumbbells, who stopped at the
purification fountain to drink straight from one of the
ladles!](./IMG_20190911_105546.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190911_114450x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Waiting at a pedestrian crossing while driving to our next stop. We only
    needed to turn right rather than cross, but it was absolute chaos once
    the gates opened and no one seemed to know who had right of way.
  </figcaption>
</figure>

![Some shop, Fujikawaguchiko: a fraction of the Fuji related handkerchiefs
sold in this place.](./IMG_20190911_120841.jpg)
![White peach and Miyazaki mango soft serve. I suspect that it was actually a
sorbet soft serve. Very fruity and refreshing in the
heat.](./IMG_20190911_121257.jpg)
![Bob and Christine's melting lavender and vanilla soft serve.](./IMG_20190911_121309.jpg)
![A baseball batting cage. Bob wanted to play but the numbers scared me off.](./IMG_20190911_123236.jpg)
![Yagizaki Park, Fujikawaguchiko: some golfing/putting event was on. I don't
get how they can stay in the sun all day](./IMG_20190911_125425.jpg)

![We drove a kilometre or two down the street, passing many workers who were
busy hand sweeping the road. Why can't my government put more money towards
stuff like this.](./IMG_20190911_132319.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190911_133128x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Sakuya Bell of Love, Fujikawaguchiko: ringing this helps with your
    relationships or grants wishes or something?
  </figcaption>
</figure>

We spent the rest of the day driving to some of the other lakes around Mount
Fuji and taking pictures.

![Lake Saiko.](./IMG_20190911_140528.jpg)
![Every lake had one of these detailed signs.](./IMG_20190911_140617.jpg)
![Lake Shoji. We watched two single kayaks racing each other in some sort of
regatta.](./IMG_20190911_142942.jpg)
![In these less busy areas around the smaller lakes, all the restaurants were
closed between lunch and dinner, so we were quite hungry. This view reminded
me of a rice ball.](./IMG_20190911_143724.jpg)

![We passed by Lake Saiko on the way home and since the sky had cleared up,
we took more pictures. I'm not sure what they use these rowboats for, but
motorboats were prohibited on this lake.](./IMG_20190911_150526.jpg)

On the drive back we saw a group of people riding bikes on the road and holding
their helmets rather than wearing them. It wasn't even a hot day and the breeze
was cool, so I'm not sure what they were thinking.

![Lawson, Fujikawaguchiko: picking up a snack at the local conbini. This
picture was taken while Bob got into a conversation with a tourist from the
USA and exchanged contact details.](./IMG_20190911_155043.jpg)
![Rakuyu, Fujikawaguchiko: a post onsen pudding which I bought from the
supermarket the night before. It was cream cheese flavoured, and absolutely
delicious. I guess there's a reason why these Ohayo puddings cost a bit
more.](./IMG_20190911_161131.jpg)
![Doing a bit of blogging in the lobby lounge area.](./IMG_20190911_173323.jpg)
!["Wow my name is up there" - Bob probably.](./IMG_20190911_180254.jpg)
![I really appreciated that they had an English menu for dinner. You can see
the Japanese menu poking out from underneath. This was our fourth kaiseki
meal that we'd eaten this trip so it was becoming slightly overwhelming since
it's such a full-on experience that also makes you very
full.](./IMG_20190911_180315.jpg)
![See the picture of the rice further down.](./IMG_20190911_181121.jpg)
![Delicious.](./IMG_20190911_180259.jpg) ![Not bad.](./IMG_20190911_181128.jpg)
![We ordered umeshu (plum wine) since we liked it two nights ago. It didn't
disappoint.](./IMG_20190911_181230.jpg)
![Not bad.](./IMG_20190911_182026.jpg) ![Delicious.](./IMG_20190911_182726.jpg)
![Interesting and delicious.](./IMG_20190911_183855.jpg)
![Delicious, although the chicken was excessively fatty.](./IMG_20190911_184600.jpg)
![The cold stuff served with green tea and miso soup.](./IMG_20190911_184623.jpg)
![Fruit is such a luxury here that a few pieces are sitting here on a plate
by themselves. Dessert was served with hojicha (roasted green tea).](./IMG_20190911_190816.jpg)
![They cut the bottom off of this grape so that it would sit on the plate
properly.](./IMG_20190911_191421.jpg)
![Back in our room. The right one was there when we got home and says
something like "I, Watanabe, cleaned your room", while the left one was there
when we came back from dinner and says something like "I've prepared your
futon, I hope the rest of your journey is good".](./IMG_20190911_193808.jpg)
![These chips that we bought last night were way too sweet.](./IMG_20190911_221346.jpg)
![Some pretty meh tasting hotel crackers.](./IMG_20190911_225233.jpg)

I'd already bathed in the afternoon so I had plenty of time after dinner to blog
and still get eight hours of sleep, although Bob then asked if I wanted to run
tomorrow morning, so we set our alarms earlier and didn't get enough sleep 😔.

##### Waking hours

0750–2330
