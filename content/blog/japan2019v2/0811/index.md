---
title:
  Tokyo looks nice at night because you can't see the ugly mish-mash of
  buildings
date: "2019-08-11"
---

I got out of bed at 1330 and studied some Japanese, before having a tomato ramen
breakfast at 1600 with Elbert in Ootsuka. We went home and I figured out how to
use my gimbal properly, although I've realised that I don't really have an
appropriate bag for it to protect the lens, although the lens is somewhat
recessed into the housing anyway.

![Some ramen shop, Ootsuka: tomato ramen.](./IMG_20190811_161049.jpg)
![Rice bowl for DIY risotto.](./IMG_20190811_162056.jpg)
![Post DIY.](./IMG_20190811_162706.jpg)
![Home, Ootsuka: playing around with my gimbal and action cam. The gimbal really
does make videos so stable, it's a night and day difference.](./IMG_20190811_172434.jpg)
![Protecting my lens using a glasses case so that I could stuff it in my backpack.](./IMG_20190811_195148.jpg)

At night I headed to the Tokyo Government Metropolitan Building to go to the
observation deck, taking my action cam and gimbal with me like a good tourist.
My expectations weren't particularly high because it wasn't one of the paid
towers like Tokyo Tower/Skytree and I've never really been amazed at an
observation deck before[^1], so I was pleasantly surprised when I saw the view.
The high density of the city lights extending as far as the eye can see, with
skyscrapers twinkling in the distance, is an awesome sight.

![Tokyo Metropolitan Government Building, Shinjuku: The internal lighting and
double glazed glass made it difficult to take good pictures. This view looks
southeast; the big dark spot on the right is Yoyogi Park.](./IMG_20190811_212929.jpg)
![Looking south-southeast towards Yokohama.](./IMG_20190811_213707.jpg)
![By comparing to the previous picture, you can see how some of the red lights
on the skyscrapers are pulsing, which made the view even cooler.](./IMG_20190811_213708.jpg)
![Tokyo Metropolitan Government Building, Shinjuku: I definitely considered
getting these from the merch shop to help me survive the next month...](./IMG_20190811_220918.jpg)
![...or these...](./IMG_20190811_220937.jpg)
![...or these too.](./IMG_20190811_220952.jpg)
![The top left stitched Maneki-Neko (beckoning cats) were so detailed that I
wanted one just for the sake of it. There was also plenty of Tokyo 2020 Olympics
merch.](./IMG_20190811_221013.jpg)
![Tokyo Metropolitan Governnment Building, Shinjuku: The view of the north and
south towers from the east.](./IMG_20190811_222540.jpg)

After that I went to Ichiran Ramen in Shinjuku, but not before crossing through
Shinjuku station and being charged 140 yen for it (I suspected I might get
charged, but I didn't have the energy to walk around trying to find the
west-east underpass which had plenty of signage up until the point where you
actually got close to the underpass, where the signage disappears for some
reason...) since it turns out that you can't get your tap on reversed like you
can with an Opal card. My IC card wouldn't even let me exit since I had just
tapped in, so I prepared a translated sentence to explain to the man staffing
the ticket barrier, but he understood right away and had decent English, so it
was for naught.

![Ichiran, Shinjuku: me probably looking silly taking this photo.](./IMG_20190811_231924.jpg)
![Ichiran ramen. You put that strange metal plate over the button behind the
bowl when you want your noodle refill. You pay for the refill, of course.](./IMG_20190811_232247.jpg)
![Same thing except now I've peeled the egg.](./IMG_20190811_232438.jpg)
![And now I've asked for a noodle refill. Damn that ramen was tasty.](./IMG_20190811_233256.jpg)
![And now I've eaten the noodle refill and drank some of that unhealthily salty
but tasty soup.](./IMG_20190811_233856.jpg)

![Home, Ootsuka: I picked up this peach alcopop from FamilyMart on the way home.
It pretty much tasted like peach tea from a bubble tea shop except I assume it
gets you drunk if you consume enough.](./IMG_20190812_002356.jpg)

Going solo with the gimbal was actually really annoying. There's no one to hold
it for you when you need to go to the bathroom or get stuff out of your bag, and
I didn't have a pouch or loop to store it - I had to put it into my backpack
carefully whenever I wasn't filming, or else just hold onto it awkwardly. I
might just take it out for cool things in the future rather than trying to
record everything like a vlog or a streamer would, since it's not like I'm going
to do much with the content anyway. I also don't have the bravery to just record
everyone and everything, for example I turned it off on the train, in the
observation deck lift, and at the cashier at FamilyMart. I don't mind looking
silly myself but it also requires being okay with making strangers somewhat
uncomfortable, even though it's probably not a big deal to them.

I still haven't washed any clothes except for the t-shirt that I wore on the
plane (since I had been wearing that for a while back in Sydney). They don't
smell and I've been changing out of them and airing them out every time I get
home (merino socks, merino undies, Uniqlo dry-ex t-shirt[^2], normal cotton
shorts (but who changes shorts every day right?). Hooray for the environment.
I'll probably change my undies though, because eww.

##### Waking hours

1200–2700

[^1]:

  not that I've been to many, only Rockefeller, Eureka Tower, Victoria Peak, and
  the Queenstown one (although I did love that one)

[^2]:

  I don't know how much is just marketing, but it really does seem to do what it
  says on the box with feeling dry and being anti-odor/anti-microbial
