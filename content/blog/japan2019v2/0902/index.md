---
title: Shabu-shabu
date: "2019-09-02"
---

I briefly woke up at noon when Elbert left the house, went back to sleep, and
when I woke up it was already late 😔. We were going to have all you can eat
shabu-shabu (a hotpot variant) at 1800 with Elbert's friend and it was already
1600, so I went to Ikebukuro early to kill some time.

![Mos Burger, Ootsuka: opening the stomach for tonight. That and the usual
necessity of eating. An excellent little 360 JPY pork teriyaki burger.](./IMG_20190902_160821.jpg)

![Coffee Valley, Ikebukuro: a good light roast Ethiopian single origin for 400
JPY. There was also a medium roast on offer. I speculate that any place that
lets you choose beans probably won't be too bad. The biscotti was a bit too hard
though and I felt bad for my teeth while biting into it after my acidic
espresso.](./IMG_20190902_165109.jpg)
![There's free WiFi too but you have to ask. I didn't bother since I wasn't
going to be there for too long.](./IMG_20190902_175107.jpg)

Then it was time for some hotpot. Elbert introduced me to Ishii, who was the
same friend that was working at [Gagana Ramen a few weeks ago](../0816/). His
English was about as good or slightly better than my Japanese, so Elbert
functioned as our translator. There were quite a few times where Ishii was
talking to me and I didn't realise, which I felt bad about.

![Syabu-yo, Ikebukuro: all you can eat vegetables is almost a bargain in and of
itself in Japan.](./IMG_20190902_201445.jpg)
![I feel like half of the veggies ended up melting into the broth or something
because I didn't actually eat that many.](./IMG_20190902_184033.jpg)
![I tried all of the random dipping things although I didn't try all of the
sauces. All delicious I'm sure.](./IMG_20190902_184659.jpg)
![Yum.](./IMG_20190902_185736.jpg)
![Elbert made me try the ponzu sauce. Not bad but I liked my sesame sauce more.](./IMG_20190902_193108.jpg)
![There was also all you can eat sushi. I tried the random ones whose names
I didn't know, and I didn't really like any of them. I did also get an eel piece
which was not bad.](./IMG_20190902_194231.jpg)
![For dessert you can make your own waffles or shaved ice with toppings.](./IMG_20190902_201433.jpg)
![Or have a soft serve. I've never used a soft serve machine before but I guess
it's unsurprisingly that it's much the same as frozen yoghurt.](./IMG_20190902_201451.jpg)
![I got a bit too greedy with the ice cream so I couldn't fit many cornflakes on
it without spilling any.](./IMG_20190902_201519.jpg)

Not pictured is some curry with rice that I tried too.

I got decently full, but not so much that I couldn't walk; partly due to eating
slowly (the limit was ninety minutes), and partly due to not feeling the need to
stuff myself anyway. Not like you really get more value out of feeling like
crap. The food was good though, and only 2267 JPY each!

After dinner we parted ways with Ishii and I bowed as we did so. I asked Elbert
smugly whether he liked my bow and he disapproving complained that I went too
low (apparently it was 60 degrees) and that Ishii didn't bow anyway.

I got home, had a long bath while learning some japanese on my phone, blogged,
and slept a bit earlier than usual. Nice!

##### Waking hours

1425–2630
