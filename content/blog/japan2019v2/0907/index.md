---
title: Eating locally on a lazy day
date: "2019-09-07"
---

Bob and Christine went to Kamakura which was even further south of Yokohama so I
couldn't really be bothered tagging along. Instead, I slept in and then ate some
local sushi train at 1630 for breakfast.

![Tenkazushi, Ootsuka: English menu.](./IMG_20190907_164135.jpg)
![Tuna.](./IMG_20190907_164304.jpg)
![Squid. A bit too slimy and tasted a little fishy.](./IMG_20190907_164713.jpg)
![Abalone I think.](./IMG_20190907_164939.jpg)
![Bintoro, I think that's the name of fatty albacore tuna.](./IMG_20190907_165155.jpg)
![Akagai (ark shell) which I ordered since I've never had it. Meh.](./IMG_20190907_165713.jpg)
![Amaebi (sweet shrimp).](./IMG_20190907_165854.jpg)
![Aji (Japanese horse mackerel).](./IMG_20190907_170104.jpg)
![Ike-hokkigai (hen clam) which I also ordered since I've never had it. Meh.](./IMG_20190907_170652.jpg)
![Hamachi (yellowtail).](./IMG_20190907_171451.jpg)

I surprised myself by eating seventeen pieces, probably becauses the pieces were
a bit smaller. Most of the plates were 125 JPY so it was pretty cheap even for
the size of the pieces, and smaller pieces means that you get to try more
different fish! I also liked that the self service hot water (which you use to
make your own tea) came out at a hot but drinkable temperature, rather than
scalding hot.

Afterwards I went to a local cafe which I'd been meaning to go to. I had an
awkward experience ordering, where I saw written in Katakana "specialty coffee"
so I asked the girl if she spoke English. She apologised and said no so I just
tried to ask what "specialty coffee" meant here and she pulled out her phone and
showed me "sour" on her translation app. I assumed that meant that she was
talking about the beans so I asked if I could have an espresso with those
specialty beans but she said that wasn't possible, which confused me and made me
realise it was probably something more to do with the preparation/brew method.

![Eightdays Cafe, Ootsuka: a medium/dark roast which didn't taste bad at
first but the aftertaste was sharp and dry rather than lingering and rich,
which made it an overall bad experience.](./IMG_20190907_173413.jpg)
![The cup had a deceptive circular rim but square base.](./IMG_20190907_173434.jpg)

I spent most of the time from 1730 to 2130 on my phone and headphones learning
Japanese. Elbert joined me later and he ordered food for his "breakfast", then
we went home and I watched some F1 qualifying and blogged.

![I ended up ordering their specialty coffee anyway since I'd already been
there for hours and I may as well try it. I didn't watch them prepare it but
I assume it was a pourover. Tasty though.](./IMG_20190907_191919.jpg)

![Home, Ootsuka: seafood and black pepper fried chicken from Lawson.](./IMG_20190907_215248.jpg)
![Some tasty onigiri from Lawson.](./IMG_20190907_220753.jpg)
![I like these ones and how they manage to keep the seaweed fresh/crunchy, but
it does come with the trade-off of getting seaweed bits everywhere.](./IMG_20190907_221537.jpg)

##### Waking hours

1430–2905
