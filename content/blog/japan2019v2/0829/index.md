---
title: Accidental lazy day
date: "2019-08-29"
---

I woke up at 1520 after turning off my alarm at 1300 and going back to sleep. I
just need to sleep earlier and have some willpower to make myself get up. The
day (and night) was spent researching for good mid-range sushi restaurants
(10000 JPY or under) or blogging.

Elbert and I went to a local onigiri shop for lunch/dinner at 1820. All of the
staff bar one were middle aged/older women who were probably mums. Watching them
make the rice balls in front of you was really cool, although I didn't get a
picture/video. They do heaps at a time, really quickly. I remember trying to
make rice balls back in Sydney and it took forever just to make one.

![Bongo, Ootsuka: huge rice ball menu. This is just one side.](./IMG_20190829_182909.jpg)
![The hot rice is great (in contrast to conbini rice balls). You can't really
tell from the picture but these were big. I was satisfied after the second one.](./IMG_20190829_183136.jpg)
![I got pickles (other side of the menu), squid (32) and kelp (35). I liked
the pickles, the squid had a bit too strong of a fermented taste, and the kelp
was average. Overall I enjoyed the meal due to the homeliness of the food and
the hot rice.](./IMG_20190829_183142.jpg)

At some point after we got home, someone rang the door and I thought that it
might be a representative of the landlord coming to ask me to leave this one
bedroom apartment. It turns out that it was just a salesman asking if Elbert
wanted better internet, but I couldn't understand their Japanese dialogue, so I
stayed hidden in the corner for literally an hour shitting myself while they had
a pleasant chat at the genkan.

![Home, Ootsuka: absolutely delicious.](./IMG_20190830_021418.jpg)

I wrote and published this blog post (2019-08-29) on the actual day that it
happened, which meant that I had to estimate when I was going to go to bed. The
estimate won't be wrong though because I'm going to make it a self-fulfilling
prophecy.

##### Waking hours

1520–2730
