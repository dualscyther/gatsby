---
title: Akihabara
date: "2019-08-25"
---

I watched TI in the early afternoon, and since all the teams that I cared about
had been knocked out, I decided not to watch the grand finals, which freed up
the rest of the afternoon and night to go to Akihabara.

It's not like it's a secret, but the public transport in Tokyo really is great.
If you want to go somewhere, you don't really need to think about what time the
train comes (unless it's late and you don't want to miss the last train). We
just missed a train as we got onto the platform, and the next train seemed to
arrive in less than a minute (which I believe is short even by Tokyo standards).

![Akihabara: it happened to be Sunday, so the main street was closed to cars.](./IMG_20190825_171825_1.jpg)

As we were taking photos of the main street, we noticed a nearby adult
store–which I've read is one of the things to see in Akihabara–so we went to
check it out even though we were dying for food. I didn't take many pictures
since there are signs all over place saying "no photos". There was a sign
explaining one of the reasons: it could potentially shame or embarrass
customers[^1], which also might explain why the two upper floors didn't permit
women from entering. Needless to say there were plenty of interesting wares.

I noticed that the majority of people in this particular store were tourists who
were probably also just there to have a look. I also saw that they sold
"supplements", and there was a sign explaining how due to some law (which I
assume they were attempting to skirt) they weren't legally allowed to say what
effects these supplements had. Sounds a bit snake oily to me.

![Some adult store, Akihabara: for some reason you could take photos of a
specific brand of products.](./IMG_20190825_172731.jpg)
![Gachapon machine for Tenga eggs.](./IMG_20190825_175353.jpg)

We were starving since it was 1830 so we found a noodle store nearby to eat
dinner (lunch? breakfast?). Elbert noticed that all the staff were Vietnamese
and actually spoke Vietnamese amongst themselves. I asked him how to say
"thanks" in Vietnamese and he fooled me for a second when he replied with "du
ma".

![Some restaurant, Akihabara: ordering off a tablet at the table rather than
prepaying at a machine up the front. I prefer this way since there are pictures
for everything, as long as the tablet/software isn't complete garbage like
you find at sushi trains in Sydney.](./IMG_20190825_183749.jpg)
![Abura soba (soup-less oil noodles) with miso soup on the side. Yummy. This
time I mixed the egg in rather than just slurping it up.](./IMG_20190825_184520.jpg)

Afterwards, we popped into an anime/games hobbyist store[^2], which had plenty
of cool stuff.

![Some anime store, Akihabara: this place had a Studio Ghibli section.](./IMG_20190825_191711.jpg)
![Just as creepy in real life.](./IMG_20190825_191952.jpg)
![Ghibli stuff.](./IMG_20190825_192014.jpg)
![Star Wars stuff.](./IMG_20190825_192304.jpg)
![Might've bought these if it was still super hot.](./IMG_20190825_192316.jpg)
![Light up chopsticks.](./IMG_20190825_192325.jpg)
![Damn that level of detail.](./IMG_20190825_192407.jpg)
![More Totoro things.](./IMG_20190825_192553.jpg)
![Assembled "paper theatre" things. You slot and glue them yourself.](./IMG_20190825_193038.jpg)
![The camera doesn't really capture the nice depth that you get from the layers.](./IMG_20190825_193050.jpg)
![Showing the layers. I bought the catbus. I wanted to buy some more as gifts
(and to make the 5000 JPY tax free threshold) but I didn't know who might want
them.](./IMG_20190825_193107.jpg)
![More cool detailed stuff.](./IMG_20190825_194247.jpg)
![Heh, this was cool.](./IMG_20190825_194424.jpg)
![Misato.](./IMG_20190825_194515.jpg)
![What made them choose these models to sell these cases 🤔](./IMG_20190825_194620.jpg)
![I don't even.](./IMG_20190825_194907.jpg)

Unfortunately the Gundam Cafe was closed by the time we got there, which was a
shame because apparently the toilets are interesting. The themed food not so
much since I don't know much about Gundam at all.

![Gundam Cafe, Akihabara.](./IMG_20190825_200715.jpg)

We popped into a nearby supermarket for some food and saw that they had 50% off
a lot of pastries as well as meat skewers and 30% off sushi. We didn't end up
getting much, I only bought a half price donut and a full price pudding. When
the checkout lady was scanning my items, she noticed that the pudding wasn't on
sale and made sure that I was okay with it, which was very thoughtful.

![Some supermarket, Akihabara: store made donut menu.](./IMG_20190825_201958.jpg)
![They like to label things with Hokkaido whenever they can. It definitely
sucked me in to make this 350 JPY purchase.](./IMG_20190825_202915.jpg)
![Glass jar, plastic lid.](./IMG_20190825_202923.jpg)
![Made on the 24th. Really tasty. This one actually had the taste of dairy which
made it taste fresh and less processed, while the Famima one and others taste
more processed. I'd still prefer the Famima one if I had to choose.](./IMG_20190825_203431.jpg)
![This donut wasn't bad, it had a cake rather than regular donut texture. Half
price stuff tastes better too.](./IMG_20190825_203636.jpg)

It was almost 2100 by the time that I finished eating my snacks at the
supermarket and everything was closing. I was surprised that everything was
closing so early (maybe it being Sunday affects it too). We'd already seen a lot
of girls in costumes trying to get people to go into their maid cafes, but on
the walk back to the station we saw a super frilly, puffy, impractical one that
just didn't look fun to walk in.

![Home, Ootsuka: a one meal day and my natural tolerance meant that this 500mL
can got me more drunk than I was expecting.](./IMG_20190825_221603.jpg)
![Pringles-like chips. But in a small can.](./IMG_20190826_003708.jpg)
![And with more packaging. They tasted like what you'd expect, which is good
since regular Japanese potato chips are expensive and not great.](./IMG_20190826_003822.jpg)

I went to bed quite drunk at around midnight but woke up at 2800 sober, so I
went on my laptop again. I took a while to finish my rather large Tokyo Midtown
blog and slept late.

##### Waking hours

1145–3105

[^1]:

  I guess while it might be more accepted in Japan (as evidenced by the sheer
  number and size of these stores) it's not like it's completely normalised
  either

[^2]:

  I wanted to call it an anime store but Elbert said that it would be more
  correct to call it a hobbyist store. This is the compromise.
