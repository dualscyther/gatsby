---
title: What clothing I've been wearing
date: "2019-08-20"
---

_I didn't pull content out of my ass to pad today's post, rather I had this
extra content sitting here and I wanted to add it to a small post so as to
maintain some consistency in reading length._

I brought more clothes than what I'm showing in this post, but these are just
the clothes that I've worn so far.

### At home

![Boxers: self explanatory, very comfortable. I'm rotating through a few pairs
of these.
T-shirt: if I'm the one that thinks the room is cold, sometimes I'll wear this.
Polyester, but breathable and stretchy.](./IMG_20190822_224512.jpg)

### T-shirts

I just realised that these are all from Uniqlo.

![Uniqlo dry-ex. 100% polyester.](./IMG_20190822_223858.jpg)
![Uniqlo dry-ex.](./IMG_20190822_224849.jpg)
![Uniqlo dry-ex t-shirt. I think this is the second generation of these t-shirts
which is why it looks different, and it feels thinner but more breathable. The
vents on the side make it look more like activewear.](./IMG_20190822_224232.jpg)
![Airism mesh. I have a feeling that this mesh version in particular is meant to
be worn as an undershirt because the thin fabric mesh makes it slightly
transparent, although it's barely noticeable with the navy colour. It seriously
feels like I'm wearing nothing. Mostly polyester.](./IMG_20190822_224205.jpg)
![This feels soft, but in my experience feels damp with less sweating, so I've
only worn it twice, when I've known that I'll be mostly indoors. 100% cotton.](./IMG_20190822_224129.jpg)

### Shorts

![I must've lost weight recently because I've needed to use the included belt.
Mostly cotton, but they're not actually that soft.](./IMG_20190822_224736.jpg)
![My most comfy shorts due to the elastic waist and softish fabric, but still
nothing like my t-shirts. Mostly cotton.](./IMG_20190822_224107.jpg)
![The pockets aren't deep enough/don't feel secure enough for me to wear them on
a day out, but I've been wearing them to walk to the local conbini because
they're soft, stretchy, and comfortable. 100% polyester.](./IMG_20190822_224218.jpg)

### Socks and undies

![Merino wool socks (I think they're 50% wool or so) and boxer briefs (mostly
wool). I need to buy some laundry powder that doesn't have softener so that I
can wash this stuff. I haven't been wearing the undies, but since I've been
wearing sandals (i.e. no socks) a decent amount and I've been airing out my
clothes as soon as I get home, I've been getting away with just wearing these
two pairs of socks.](./IMG_20190822_224336.jpg)
![(Mostly) cotton boxer briefs. Can only wear these for a day, or two at best if
it's a short, cool day and I change out of them as soon as I get home.](./IMG_20190822_224419.jpg)

### Shoes

![Michelle's Birkenstocks: very comfy, obviously breathable, great for a short
day or a walk to the conbini.
Nike Flyknit Racers: very breathable (there are literally holes) but not nice
in rain (there are literally holes).
Adidas kids Ultraboosts: good for walking around in all day due to the heel
cushioning. They're half a size small for me in Sydney which means that I
normally feel discomfort in my feet after a few hours, but I've noticed this
less in Tokyo. It's either my imagination, or I speculate that it could be
something to do with the shoe material having a higher expansion coefficient
than my feet.](./IMG_20190823_221448.jpg)

## Almost shut-in day

I normally wake up earlier than Elbert, but today it was Elbert that woke me up.
I speculate that it might've been due to the beer from last night. I watched The
International main stage (playoffs) the whole day, going out only in the break
between games: at 1745 to eat at the nearest ramen place...

![Kita Ootsuka Ramen, Ootsuka: I ordered a large ramen since I felt hungry and I
hadn't eaten all day. I normally experience this phenomenon where if I don't have
breakfast then I can't eat as much for lunch, and today was no different. The
ramen was also quite salty so I had to also drink a lot of water. Elbert had to
help me out here.](./IMG_20190820_175343.jpg)
![I bought this yesterday with the cherry tomatoes, since I felt like fruit.](./IMG_20190820_193244.jpg)
![I bought this yesterday with the Pino ice cream things, since I felt like
dessert.](./IMG_20190820_215436.jpg)

...and once at around midnight to buy some food from Lawson for dinner.

![Peperonchino pasta from Lawson. Seriously so delicious, great value for taste.](./IMG_20190820_235932.jpg)
![Watching Mineski vs Navi. Elbert made me tea.](./IMG_20190821_000251.jpg)

You'd think that since I stayed home all day I would've been able to get my
blogging done early but I somehow still ended up sleeping pretty late.

##### Waking hours

1530–2900
