---
title: Another shopping fail
date: "2019-09-19"
---

I went to the west side of Ikebukuro station for the first time today. It seemed
like more of the same, with plenty of department stores and malls.

![Some shop, Ikebukuro: grilled chicken don for lunch at 1550 after some
window shopping.](./IMG_20190919_155348.jpg)

One difference was that there was only one Bic Camera store rather than four!
The Bic Camera West store actually had everything that I wanted to buy except
for a cheap fountain pen that I'd seen in Tokyu Hands which I'd read was a good
beginner pen, so I decided to wait and see if the main store had it instead.

I did some tax free shopping at Don Quijote, buying extra snacks for the house
to make it to the 5000 JPY tax free threshold. The staff at the tax free counter
all spoke Mandarin to each other, so I guess Don Quijote understands the
demographics of their customers.

Afterwards I looked for the pen that I wanted at Bic Camera but they didn't
stock it. I went ahead and collected up everything that I wanted to buy anyway,
and then went up to the top floor to buy a watch that I'd been eyeing, along
with a leather strap for my existing watch. I asked the salesman if they'd
change my band for free and he told me that it would be fine except for the fact
that it was just past 2100 and their watch repairs counter had closed. Ugh. I
would've bought everything else then and there, except that if you spend over
30000 JPY then the Facebook coupon gets you 7% off rather than 5% off, and I
realised that I was close to that limit anyway. I left all my hard-collected
items again, resolving to come back another day.

On the way back down the building I asked a staff member whether I could
purchase alcohol as part of my 30000 JPY, or whether it had to be bought
separately at the alcohol counter. He said no, and that alcohol doesn't quality
for a discount anyway. I told him that I understand but I don't have 30000 JPY
of things to buy. I think I noted this interaction down because it felt good
that we managed to talk and only use a tiny bit of English.

![Caffe Veloce, Ikebukuro: a ham, cheese and lettuce sandwich for a
snack/dinner. I would've gone to Coffee Valley again but they closed at 2200
and it was 2145 already and I wanted to learn some Japanese on my
phone.](./IMG_20190919_214717.jpg)

I don't feel like I did much today but I dragged it out long enough that my feet
were a bit tired. My new Birkenstocks are comfortable for a few hours but I
noticed the lack of support after a long day. My ankles were tired too, which is
a first for me.

![Home, Ootsuka: that sandwich wasn't enough.](./IMG_20190920_000212.jpg)
![I didn't know what to expect but this was great.](./IMG_20190920_001705.jpg)
![And this was not bad. I wish it was a dark chocolate coating.](./IMG_20190920_033854.jpg)

##### Waking hours

1130–2850
