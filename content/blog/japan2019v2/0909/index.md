---
title: Roadtripping day 1
date: "2019-09-09"
---

We saw that a lot of trains were cancelled due to last night's typhoon so we
checked out late at 1100. Out on the street you wouldn't even be able to tell
that Typhoon Faxai had come through other than a higher than usual number of
people sweeping leaves out the front of their buildings.

![Hotel Vista, Minatomirai: a Tim Tam that Bob and Christine saved from their
flight.](./IMG_20190909_104608.jpg)
![Outside 7-Eleven, Yokohama station: a quick snack for breakfast.](./IMG_20190909_111316.jpg)

On the other hand, it was a mess inside Yokohama station. Many train lines were
cancelled indefinitely, and others were delayed. There were queues inside both
the JR and private stations waiting for lines to reopen. The JR station in
particular was hot and stuffy and it really would've sucked to be one of the
people waiting around.

We went to a tourist information kiosk inside the station to ask how to get to
Odawara station–where our car rental was waiting–and they advised us that the
Sotetsu line had just opened so we should try to confirm with them. When we got
there, they told us that there line was still closed and that we should try to
catch one of the JR local lines. But then at the JR kiosk, they told us that the
local line wasn't running yet, although a shinkansen was available from
Shin-Yokohama station although it would be ~3000 JPY rather than ~800 JPY.

We decided to have lunch first to see if things got better (it was already
midday), although we didn't mind taking the shinkansen if it got us there
faster. As long as we got to the car rental by 1500 it would be fine.

![Yokohama station, Yokohama: one of a few queues that we
saw.](./IMG_20190909_112321.jpg)
![Yokohama station, Yokohama: udon with some deliciously sweet boiled
veggies.](./IMG_20190909_122712.jpg)

We finished lunch but the local JR Tokaido line to Odawara still wasn't running,
so we caught the train to Shin-Yokohama station to take the shinkansen. Two
stops away from Yokohama we realised we'd actually caught the wrong train (right
platform but wrong train) so we waited ten minutes in the heat and caught the
train back to Yokohama. Luckily, by the time we got there, the Tokaido line had
opened so no shinkansen needed!

The train took about fifty minutes and we arrived at 1500 rather than 1100 as we
had planned. We picked up the car and drove to our ryokan. It was somewhat
anxiety inducing driving on really narrow roads with blindspots around every
corner. I also found it refreshing seeing houses everywhere rather than rows of
apartments and skyscrapers.

![Odawara station, Odawara: Christine's been stamping her book every time
that she sees a stamp.](./IMG_20190909_144259.jpg)

<figure className="blogpost-video">
  <video controls >
    <source src="VID_20190909_155353x264.mp4" type="video/mp4">
  </video>
  <figcaption>
    Gora station, Hakone: after we crossed this we accidentally turned left
    into a bus turntable area, and the traffic guy got really mad. It's the
    first time I've seen an angry Japanese person here.
  </figcaption>
</figure>

![Lalaca, Hakone: finally here. What a long day.](./IMG_20190909_155907.jpg)

Checking in was easy because our receptionist spoke English well and was able to
answer our questions and explain the facilities to us.

![View from the lobby.](./IMG_20190909_160321.jpg)
![View from the room.](./IMG_20190909_161235.jpg)
![View of the room.](./IMG_20190909_161327.jpg)
![Complimentary room sweets.](./IMG_20190909_162225.jpg)
![Folded yukatas in our room.](./IMG_20190909_174432.jpg)
![Lots of yukatas for women to choose from. Guys only get the one option.](./IMG_20190909_164756.jpg)

![Hakone somewhere: we drove to somewhere close because it was 1630 already
and dinner was at 1800.](./IMG_20190909_170401.jpg)
![Gora station: we got back to the hotel with time to spare, so we walked
down to the local shops.](./IMG_20190909_172203.jpg)
![Hot red bean bun. Yummy, especially since it was hot.](./IMG_20190909_172851.jpg)
![Nice tree.](./IMG_20190909_173354.jpg)
![A small local shrine just by the road.](./IMG_20190909_173655.jpg)

Then it was time for dinner. Our waiter couldn't really speak English but he was
half Japanese half Taiwanese, so he was able to communicate to Bob in Mandarin.

![Lalaca, Hakone: kaiseki dinner. Our starters were waiting for us when we
sat down.](./IMG_20190909_180526.jpg)
![ ](./IMG_20190909_180950.jpg)
![We ordered umeshu (Japanese plum wine).](./IMG_20190909_181042.jpg)
![Sashimi.](./IMG_20190909_181634.jpg)
![I forget what this was to be honest, but it was tomatoey and the fish
tasted good.](./IMG_20190909_182110.jpg)
![A better view after stirring it a bit.](./IMG_20190909_182504.jpg)
![Somewhat unnecessary presentation.](./IMG_20190909_182234.jpg)
![Crab with a sweet jelly-like thing.](./IMG_20190909_182443.jpg)
![Then we had shabu-shabu. Here is ponzu and sesame sauce.](./IMG_20190909_183408.jpg)
![Daikon radish and spring onion.](./IMG_20190909_183550.jpg)
![Fuji pork (10s), noodles (1 minute), mushrooms and veggies (3 minutes). I
was glad that they told us how long to cook each thing
for. We also had rice with the shabu-shabu soup at the end.](./IMG_20190909_183555.jpg)
![Sardine, eggplant and chilli tempura.](./IMG_20190909_185831.jpg)
![Pickles.](./IMG_20190909_185837.jpg)
![Meatball soup with tofu skin.](./IMG_20190909_185857.jpg)
![I like these rice balls which you can hold by the bottom like this.](./IMG_20190909_191052.jpg)
![Mango pudding with hojicha (roasted green tea).](./IMG_20190909_191934.jpg)
![Futons made! They move the table and make your futons for you while you're
having dinner, and then remove the futons during
breakfast.](./IMG_20190909_194410.jpg)
![Each room could book the private onsen for half an hour so we didn't let it
go to waste. It was outdoors which would've been nice, except there were
mosquitoes so we had to keep our bodies submerged to not get bitten, but the
water was really hot (not more than usual though I suppose), so I couldn't
really bear being there for very long.](./IMG_20190909_210616.jpg)
![More complimentary room sweets.](./IMG_20190909_221607.jpg)

##### Waking hours

1000–2400
